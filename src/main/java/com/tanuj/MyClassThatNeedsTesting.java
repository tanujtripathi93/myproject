package com.tanuj;

public class MyClassThatNeedsTesting {

    public String myMethod() {

        String string = getDummyString();
        return string;
    }

    private String getDummyString() {
        // modified
        return "string";
    }

    private String hello() {
        return "hello World 1";
    }
}
