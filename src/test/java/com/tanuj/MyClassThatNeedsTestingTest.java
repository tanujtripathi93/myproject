package com.tanuj;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(MockitoJUnitRunner.class)
class MyClassThatNeedsTestingTest {

    @InjectMocks
    private MyClassThatNeedsTesting myClassThatNeedsTesting;

    @Test
    public void myMethodTest() {
        MyClassThatNeedsTesting myClassThatNeedsTesting = new MyClassThatNeedsTesting();
        String s = myClassThatNeedsTesting.myMethod();
        File file = new File("a.txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals("string", s);
    }
}